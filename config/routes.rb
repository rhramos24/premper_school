Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  namespace :setup do
    resources :countries do
      resources :states
    end
    resources :positions
    resources :sectors
    resources :departments
    
    resources :companies do
      resources :employeess
    end
  end
end
