class Department < ActiveRecord::Base
	has_many :employees, dependent: :nullify
	has_many :positions, through: :employees
	validates :name, presence: true, length:{maximum: 20}
	validates :code, presence: true, length:{maximum: 10}, uniqueness: true  
end