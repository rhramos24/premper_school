class Address < ActiveRecord::Base
  belongs_to :addresseable, polymorphic: true
  validates :summary,:country_id, presence: true
end