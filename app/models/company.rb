class Company < ActiveRecord::Base
  has_many :employees, dependent: :destroy
  belongs_to :sector
  belongs_to :country
  has_many :phones, as: "phoneable", dependent: :delete_all

  validates :name, presence: true,
    length: {in: 3..25},
    uniqueness: { 
      scope: :country_id, message: I18n.t("activerecord.errors.messages.company_exists"), 
      case_sensitive: false 
    }
  validates :sector_id, :country_id, presence: true
end