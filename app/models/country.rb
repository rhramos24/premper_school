class Country < ActiveRecord::Base
  has_many :states, dependent: :delete_all
  has_many :companies, dependent: :nullify
  validates :name, presence: true, length:{maximum: 30}, uniqueness: true
  validates :iso_2, presence: true,length:{is: 2}, uniqueness: true
  validates :iso_3, presence: true,length:{is: 3}, uniqueness: true
end