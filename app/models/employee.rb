class Employee < ActiveRecord::Base
  belongs_to :company
  belongs_to :position
  belongs_to :department
  has_many :phones, as: "phoneable", dependent: :delete_all
  has_many :addresses, as: "addresseable", dependent: :delete_all
  belongs_to :boss , class_name: "Employee"

  validates :first_name,:last_name, 
            presence: true,
            length:{maximum: 25}
  validates :company_id,:position_id,:department_id, presence: true
  validates :code, presence: true, uniqueness: true,length: {maximum: 10}
  validates :email, email: true
  validates :boss_id, presence: true, unless: :not_ceo?
  validate  :off_working_time

  private
  def not_ceo?
  	boss_id.blank? and code.eql?("CEO")
  end

  def off_working_time
    errors.add(:created_at, I18n.t("activerecord.errors.messages.off_working_time")) if Time.now.hour > 18
  end
end