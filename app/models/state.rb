class State < ActiveRecord::Base
  belongs_to :country
  validates :name, presence: true, length:{maximum: 25}
  validates :code, presence: true, length:{maximum: 10}, uniqueness: true  
  validates :country_id, presence: true
end