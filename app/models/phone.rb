class Phone < ActiveRecord::Base
  belongs_to :phoneable, polymorphic: true
  validates :number, presence: true,
  		    length: {maximum: 10},
  		    phone: true
  validates :kind, presence: true, length: {maximum: 3}
end