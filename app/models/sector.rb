class Sector < ActiveRecord::Base
  has_many :companies
  validates :name, presence: true, length:{maximum: 20}, uniqueness: true
  validates :code, presence: true,length:{maximum: 10}, uniqueness: true
end