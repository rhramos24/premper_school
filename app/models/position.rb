class Position < ActiveRecord::Base
  has_many :employees, dependent: :nullify
  has_many :departments, through: :employees
  validates :name, presence: true, length:{maximum: 20}, uniqueness: true
  validates :code, presence: true,length:{maximum: 10}, uniqueness: true
end