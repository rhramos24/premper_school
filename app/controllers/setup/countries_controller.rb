class Setup::CountriesController < ApplicationController
  respond_to :html, :json, :xml

  def index
    @countries = Country.order('created_at DESC')
    respond_with(@countries)
  end
  
end
