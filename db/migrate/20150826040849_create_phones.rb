class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :number, length: 10, null: false
      t.string :kind, length: 3, null: false
      t.references :phoneable, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end
