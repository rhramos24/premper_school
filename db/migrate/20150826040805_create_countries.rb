class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name, length: 30, nul: false
      t.string :iso_2, length: 2, null: false,uniqueness: true
      t.string :iso_3, length: 3, null: false,uniqueness: true
      t.timestamps
    end
  end
end
