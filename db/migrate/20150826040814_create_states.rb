class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name, length: 25, null: false
      t.string :code, length: 10, null: false, uniqueness: true
      t.belongs_to :country, null: false, index: true
      t.timestamps
    end
  end
end
