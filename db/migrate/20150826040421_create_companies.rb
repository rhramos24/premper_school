class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name, length: 25, null:  false
      t.belongs_to :sector, null: false, index: true
      t.belongs_to :country, null: false, index: true
      t.timestamps
    end
  end
end
