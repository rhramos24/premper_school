class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.string :name, length: 20, null: false
      t.string :code, length: 10, null: false, uniqueness: true
      t.timestamps
    end
  end
end
