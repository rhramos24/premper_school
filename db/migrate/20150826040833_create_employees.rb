class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :first_name, length: 25, null: false
      t.string :last_name, length: 25, null: false
      t.belongs_to :company, null: false, index: true
      t.belongs_to :position, null: false, index: true
      t.string :email, length: 30, null: false, uniqueness: true
      t.string :code, length: 10, null: false, uniqueness: true
      t.belongs_to :boss, index: true
      t.belongs_to :department, null: false, index: true
      t.timestamps
    end
  end
end
