class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.text :summary, null: false
      t.integer :country_id, null: false, index: true
      t.references :addressable, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end
